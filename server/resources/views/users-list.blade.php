<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>users-list</title>
</head>
<body>
@include('dashboard')
<div class="top-links" style="display: flex; justify-content: space-between;padding: 40px 0;">
    <p><a href="/dashboard" style="font-weight:bold; font-size: 20px">go to dashboard</a></p>
    <p style="margin: 0 150px 0 0" ><a href="/users/create" class="add-user" style="font-weight:bold; font-size: 20px">add new user</a></p>
</div>

        <h1 style="text-align:center">Список пользователей</h1>

        <form action="/users/search" method="post" style="display:flex;align-items:end; float: right; margin-right: 150px;">
                <div class="mb-3">
                    <input type="text" class="form-control" id="search" placeholder="enter user name" name="search">
                </div>
                <div class="buttons-wrapper" style="text-align: center;">
                    <button type="submit" class="btn btn-primary btn-md">Search</button>
                </div>
            </form>

            <table class="productslist" style="margin:0 auto; width:80%">
                <tr style="text-align: left">
                    <th>number</th>
                    <th>id</th>
                    <th>status</th>
                    <th>first name</th>
                    <th>last name</th>
                    <th>phone number</th>
                    <th>e-mail</th>
                    <!-- <th>actions</th> -->
                </tr>
                @php
                    $i=0;
                @endphp
                    @foreach ($users as $user) 
                    @php
                        $i=$i+1;
                    @endphp
                            <tr>
                                <td>{{$i}}</td>
                                <td>{{$user->id}}</td>
                                <td>{{$user->status}}</td>
                                <td>{{$user->first_name}}</td>
                                <td>{{$user->last_name}}</td>
                                <td>{{$user->phone_number}}</td>
                                <td>{{$user->email}}</td>
                                <td>
                                    <a href="">edit</a>
                                </td>
                                <td>
                                    <a href="{{'delete/'.$user->id}}" >delete</a>
                                </td>
                                <td>
                                    <a href="">view</a>
                                </td>
                            </tr>
                    
                    @endforeach
    </table>

    <nav aria-label="Page navigation example">
    <ul class="pagination justify-content-center" style="margin-top: 50px;">
        <li class="page-item disabled">
        <a class="page-link" href="#" tabindex="-1" aria-disabled="true">Previous</a>
        </li>
        <li class="page-item"><a class="page-link" href="#">1</a></li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item"><a class="page-link" href="#">3</a></li>
        <li class="page-item">
        <a class="page-link" href="#">Next</a>
        </li>
    </ul>
    </nav></div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>