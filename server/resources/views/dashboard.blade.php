<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Dashboard</title>
</head>
<body>
    <wrapper>
    <header style="padding:10px 50px; background:#d3a7ec">
        <ul class="nav justify-content-end">
            <li class="nav-item" style=""color>
                <a class="nav-link active" style="color:white; font-weight:bold" href="#">Admin Name</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" style="color:white; font-weight:bold" href="#">Edit Profile</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" style="color:white; font-weight:bold" href="#">Log Out</a>
            </li>
        </ul>
    </header>
   
        <aside style="float: left; margin-top: 100px;">
            <nav class="nav flex-column">
                <a class="nav-link active" href="#">Статистика</a>
                <a class="nav-link" href="#">Заказы</a>
                <a class="nav-link" href="/goods/list">Товары</a>
                <a class="nav-link" href="/users/list">Пользователи</a>
                <a class="nav-link" href="/admins/list">Администраторы</a>
            </nav>
        </aside>

    
        
   

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>