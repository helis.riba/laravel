<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function list() {
        return view('users-list', ['users' => User::all()]);
        //return view('users-list', ['users' => User::paginate(15)]);
    }

    public function create() {
      
        return view('user-create');
        
    }

    public function post(Request $request)
    {
        $user = new User();
        $user->status=$request->status;
        $user->first_name=$request->first_name;
        $user->last_name=$request->last_name;
        $user->phone_number=$request->phone_number;
        $user->email=$request->email;

        $user->save();

        return redirect('/users/list');
    }

    public function delete($id) 
    {
        $user = User::find($id);  

        $user->delete();
        
        return redirect('/users/list');
    }

}
