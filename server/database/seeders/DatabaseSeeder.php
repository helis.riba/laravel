<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Good;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         \App\Models\User::factory(30)->create();
        \App\Models\Good::factory(30)->create();
        \App\Models\Admin::factory(30)->create();
    }
}
